trombilinux
===========

While usable, this software is not developped anymore: please see
[WhosWho](https://framagit.org/Yvan-Masson/WhosWho) for a possible alternative.

Trombilinux aims at creating simple face books / trombinoscope: a sheet
containing faces and corresponding names, typically used in schools.

It is a command line tool. The only free (as in freedom) equivalent I know is
[Blocks: faces](https://moodle.org/plugins/view.php?plugin=block_faces), a
Moodle plugin.

Features:

- Source pictures can be automatically cropped around face (square or 3/4
  format) using face detection.
- Names can be gathered from picture names or from a CSV file.
- Faces can be sorted by picture name or by sorting the CSV.
- Multiple formats and DPI are available (A4 and A3 portrait or landscape, 4x5,
  5x6…), and other can be easily added.
- Generates multipage PDF (or any formats supported by ImageMagick) if needed
- When using a CSV file, a missing picture is replaced by a default image.


Examples
========

- `Example.pdf` was created by running
  `trombilinux example/ -t "Famous people" -o Example.pdf`
- `Example_with_csv.pdf` was created by running
  `trombilinux example/ -n example_names.csv -o "Example_with_csv.pdf"`
- `Example_cropped_3-4.pdf` was created by runing
  `trombilinux example/ --crop -t "Famous people" -o "Example_cropped_3-4.pdf"`,
  manual selection of faces that were wrongly detected (see [Issues](#issues)),
  and a second run with
  `trombilinux example/cropped/ -t "Famous people" -o "Example_cropped_3-4.pdf"`
- `Example_cropped_square_4x5_300dpi.pdf` was created by running
  `trombilinux example/ --crop -t "Famous people" -f "a4,portrait,4x5,square,300dpi" -o "Example_cropped_square_4x5_300dpi.pdf"`,
  manual selection of faces that were wrongly detected (again see
  [Issues](#issues)), and a second run with
  `trombilinux example/cropped/ -t "Famous people" -f "a4,portrait,4x5,square,300dpi" -o "Example_cropped_square_4x5_300dpi.pdf"`


Usage
=====

Please run `trombilinux --help`.


Dependencies
============

Trombilinux has the following dependencies:
- Python 3
- The following Python 3 libraries:
  - Willow
  - PIL
  - OpenCV
- `montage` and `convert` commands from ImageMagick

This program has only been tested on Debian Buster (current testing), where you
must install:
- python3
- python3-willow
- python3-pil (normally automatically installed by python3-willow)
- python3-opencv
- imagemagick

This program might work on other platform, but I did not care to make it
portable.


Issues
======

You can consider this program as a proof of concept, as it has many issues:

- Face detection works better if the face is in front of the camera and if the
  person is over a blank background. Even with good pictures, the program often
  detects more than one face, or fails to detect one:
  - If it does not detect any face, manually crop the picture and put it in the
  "cropped" directory created on first run.
  - If it detects more than one face but uses the wrong one, simply rename the
  picture in the "cropped" subdirectory.
  - Then, re-run the program without face detection and using pictures directly
  from the "cropped" directory.
- I am not a developper, so there are probably many bugs. Notably, there is
  almost no error handling if you provide bad parameters.


For contributors
================

If you want to check output PDF resolution (for example to define new sheet
formats), you can use `pdfimages` command with `-list` option (from the
poppler-utils package on Debian).


Credits
=======

`default_face_3-4.jpg` and `default_face_square.jpg` are modified versions of
the picture found on
http://maxpixel.freegreatpicture.com/Avatar-Grey-Account-User-Person-Operating-System-1699635,
and thus are also distributed under Creative Commons Zero - CC0 licence.
